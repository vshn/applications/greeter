pages   := $(shell find . -type f -name '*.adoc')
out_dir := ./docs
docker_cmd  ?= podman
docker_opts ?= --rm --tty # add --user "$$(id -u)" if using docker!

antora_cmd  ?= $(docker_cmd) run $(docker_opts) --volume "$${PWD}":/antora antora/antora:2.3.3

vale_cmd ?= $(docker_cmd) run $(docker_opts) --volume "$${PWD}"/docsrc/modules/ROOT/pages:/pages vshn/vale:2.6.1 --minAlertLevel=error /pages

hunspell_cmd ?= $(docker_cmd) run $(docker_opts) --volume "$${PWD}":/spell vshn/hunspell:1.7.0 -d en,vshn -l -H $(out_dir)/**/*.html

preview_cmd ?= $(docker_cmd) run --rm --publish 35729:35729 --publish 2020:2020 --volume "${PWD}":/preview/antora vshn/antora-preview:2.3.4 --antora=docsrc --style=antora

htmltest_cmd ?= $(docker_cmd) run $(docker_opts) --volume "$${PWD}"/$(out_dir):/test wjdp/htmltest:v0.12.0

.PHONY: all
all: html

.PHONY: clean
clean:
	rm -rf $(out_dir)

.PHONY: vale
vale:
	$(vale_cmd)

.PHONY: spell
spell: html
	$(hunspell_cmd)

.PHONY: preview
preview:
	$(preview_cmd)

.PHONY: htmltest
htmltest: html
	$(htmltest_cmd)

.PHONY: html
html: $(out_dir)/index.html

$(out_dir)/index.html: playbook.yml $(pages)
	$(antora_cmd) $<
